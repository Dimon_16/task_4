﻿(function () {
    'use strict';
    function appService($http) {
        var factory = {};
        factory.allNotification = [];

        return factory;
    };
    appService.$inject = ['$http'];
    angular.module('app.service', [])
    .factory('appService', appService);

})();