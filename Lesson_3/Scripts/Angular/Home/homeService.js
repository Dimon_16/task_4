﻿(function () {
    'use strict';
    function homeService($http) {
        var factory = {};
        factory.allposts = [];
        factory.gethomeResults = function (nextPosts) {
            //$http.get("/Home/GetPosts").then(function (data) {
            //    factory.allposts.push(factory.allposts, data);
            //});
            if (nextPosts === undefined || nextPosts === null) {
                return $http.get("/Home/GetPosts");
            }
            else {
                return $http.get("/Home/GetPosts", { page: nextPosts });
            }

        }

        factory.deletePost = function (postId) {
            return $http.post("/Posts/Delete", { id: postId });
        }

        factory.changePostValue = function (postModel) {
            return $http.post("/Posts/Edit", { post_changes: postModel });
        }

        factory.addLikeToPost = function (postId) {
            return $http.post("/Posts/LikingPost", { postID: postId });
        }

        factory.dislikingPost = function (postId) {
            return $http.post("/Posts/DislikingPost", { postID : postId});
        }

        return factory;
    };

    homeService.$inject = ['$http'];

    angular
      .module('home.service', [])
      .factory('homeService', homeService);
})();