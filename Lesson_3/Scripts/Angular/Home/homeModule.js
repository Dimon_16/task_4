﻿(function () {
    angular.module('app.home', [])
        .config(['$stateProvider', function ($stateProvider) {
            $stateProvider.state('home', {
                url: "/home",
                template: '<home-component></home-component>',
                data: {
                    pageTitle: 'Home',
                }
            });

        }]);
})();