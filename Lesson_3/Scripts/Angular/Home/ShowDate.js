﻿angular.module('app').filter("showDate", function () {
    return function (x) {
        return new Date(parseInt(x.substr(6)));
    }
});