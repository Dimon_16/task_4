﻿(function () {
    'use strict';
    function homeController(homeService, realTimeService, $uibModal, $scope) {
        var vm = this;
        vm.service = homeService;
        vm.show = true;
        vm.getResults = function () {
            homeService.gethomeResults().success(function (data) {
                for (var i = 0; i < data.length; i++) {
                    data[i].DateOfCreate = data[i].DateOfCreate.replace("/Date(", "").replace(")/", "");
                }
                homeService.allposts = data;
            });
        }
        vm.deletePosts = function (postId) {
            homeService.deletePost(postId).success(function (data) {
                for (var i = 0; i < homeService.allposts.length; i++) {
                    if (homeService.allposts[i].ID === postId) {
                        homeService.allposts.splice(i, 1);
                    }
                }
            });
        }

        vm.openPopup = function (postModel) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: '/Scripts/Angular/HomeHTML/ModalEdit.html',
                controller: function ($scope, $uibModalInstance) {
                    $scope.Title = postModel.Title;
                    $scope.Description = postModel.Text;
                    $scope.close = function () {
                        $uibModalInstance.dismiss('cancel');
                    }
                    $scope.send = function () {
                        postModel.Title = $scope.Title;
                        postModel.Text = $scope.Description;
                        homeService.changePostValue(postModel).success(function () {
                            for (var i = 0; i < homeService.allposts.length; i++) {
                                if (homeService.allposts[i].ID === postModel.ID) {
                                    homeService.allposts[i] = postModel;
                                }
                            }

                        });
                        $scope.close();
                    }
                }
            });
        }

        vm.likePost = function (postModel) {
            debugger;
            realTimeService.invoke('LikingPost', postModel.ID);
            }

        vm.dislikePost = function (postModel) {
            debugger;
            realTimeService.invoke('DislikingPost', postModel.ID);
        }

        realTimeService.on("likingPost", function (data) {
            debugger;
            for (var i = 0; i < homeService.allposts.length; i++) {
                if (homeService.allposts[i].ID == data.ID) {
                    homeService.allposts[i] = data;
                    homeService.allposts[i].isLike = true;
                    homeService.allposts.Likes.length++;
                }
            }
        });

        realTimeService.on("dislikingPost", function (data) {
            debugger;
            for (var i = 0; i < homeService.allposts.length; i++) {
                if (homeService.allposts[i].ID == data.ID) {
                    homeService.allposts[i] = data;
                    homeService.allposts[i].isLike = false;
                    homeService.allposts.Likes.length--;
                }
            }
        });

    }

    homeController.$inject = ['homeService', 'realTimeService', '$uibModal', '$scope'];
    angular
        .module('app.home')
        .component('homeComponent', {
            templateUrl: '/Scripts/Angular/HomeHTML/homeTest.html',
            controller: homeController,
            bindings: {

            }
        })
        //.component('post',{]);
})();