﻿(function () {
    'use strict';

    function postController(homeService) {
        console.log(this.post);
    }
    postController.$inject = ['homeService'];
    angular.module('app.home')
    .component('postComponent', {
        templateUrl: '/Scripts/Angular/HomeHTML/Post.html',
        controller: postController,
        bindings:{
            post: '<'
        }    
    })
})();