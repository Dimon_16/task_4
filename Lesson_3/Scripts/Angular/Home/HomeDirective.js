﻿(function () {
    'use strict';
    function loadingPosts(homeService) {
        var d = 1;
        var a = 0;
        return {
            restrict: 'AE',
            scope: true,
            link: function (element, attrs, ngModel) {
                window.onscroll = function () {
                    var scrolled = window.pageYOffset || document.documentElement.scrollTop;
                    //console.log("d = " + d);
                    //console.log(scrolled + "px" + "height" + $(document).height());
                    //console.log($(window).height());
                    if ($(window).scrollTop() >= $(document).height() - 900) {
                        if (a == 0) {
                            a = 1;
                            homeService.gethomeResults(d).success(function (data) {
                                for (var i = 0; i < data.length; i++) {
                                    data[i].DateOfCreate = data[i].DateOfCreate.replace("/Date(", "").replace(")/", "");
                                }
                                homeService.allposts.push.apply(homeService.allposts, data);
                                a = 0;
                            });
                            d++;
                        }
                    }
                }
            }
        }
    }
    loadingPosts.$inject = ['homeService'];
    angular.module('home.directive', [])
    .directive('loadingPosts', loadingPosts);
})();