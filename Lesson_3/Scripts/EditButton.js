﻿$(function () {
    $(".bat_2").click(function () {
        var modal = document.getElementById('myModal');
        var button = document.getElementById('ExitButton');
        modal.style.display = "block";
        button.onclick = function () {
            modal.style.display = "none";
        }
        var id = $(this).attr("id").replace("bat_2_", "");
        var Title = $("#title_" + id).text();
        var Text = $("#text_" + id).text();
        console.log("title = " + Title);
        console.log("id = " + id)
        $("#titleid").val(Title);
        $("#descriptionid").val(Text);
    });
    $("#SendButton").click(function () {
        var ID = $("#postId").val();
        var Title = $("#titleid").val();
        var Text = $("#descriptionid").val();
        console.log("id = " + ID);
        $.ajax({
            type: "POST",
            url: "/Home/SaveChanges",
            data: { Title: Title, Text: Text, ID: ID },
            datatype: "html",
            success: function (data) {
                $("#title_" + ID).html(Title);
                $("#text_" + ID).html(Text);
            }
        });
    });
});