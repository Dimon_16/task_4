﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Lesson_3.Startup))]
namespace Lesson_3
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
