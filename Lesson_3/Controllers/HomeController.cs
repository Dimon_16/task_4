﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Lesson_3.Models;
using System.Data.Entity;
using Microsoft.AspNet.Identity;

namespace Lesson_3.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Index()
        {
            //var postsAutor = db.Posts.Include(u => u.Autor).ToList();
            return View();
        }

        public JsonResult GetPosts(int page = 0, int count = 10)
        {
            var list = db.Posts.Include(_ => _.Autor).Include(_ => _.Likes).OrderByDescending(_ => _.ID).ToList().Skip(page * count).Take(count).ToList();
            //string query = "select top 10 AspNetUsers.FirstName from Posts left join AspNetUsers on(Posts.Autor_ID = AspNetUsers.id)  where Posts.ID < " + a +" order by  Posts.ID desc";
            /*var allPosts = db.Database.SqlQuery<Post>(query);*/ /*db.Posts.Include(x=>x.Autor)/*.Take(a)*//*.ToList();*/
            string currentUser = User.Identity.GetUserId();
            foreach (var post in list)
            {
                post.isLike = post.Likes.FirstOrDefault(x => x.personID == currentUser) != null;
            }
            var answer = new JsonResult() { Data = list, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            return answer;
        }

        public JsonResult GetNotification()
        {
            string currentUser = User.Identity.GetUserId();
            var noteList = db.Notifications.Include(x => x.Autor_Id).Include(x => x.Person_Id).Include(x => x.LikedPost).ToList().Where(x=>x.Autor_Id.Id == currentUser).OrderByDescending(x => x.ID).ToList();
            return new JsonResult() { Data = noteList, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public void SaveChanges(Post post)
        {
            Post newpost = new Post();
            newpost = db.Posts.Find(post.ID);
            newpost.Title = post.Title;
            newpost.Text = post.Text;
            db.SaveChanges();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        //[HttpPost]
        //public ActionResult PostSearch(string name)
        //{
        //    var allpost = db.Posts.Where(a => a.Title.Contains(name)).ToList();
        //    var postsAutor = db.Posts.Include(u => u.Autor).ToList();
        //    if (allpost.Count <= 0)
        //    {
        //        return RedirectToAction("/Contact/Index");
        //    }
        //    return PartialView(allpost);
        //}
    }
}