﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Lesson_3.Models;
using Microsoft.AspNet.Identity;

namespace Lesson_3.Controllers
{
    public class PostsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Posts
        public ActionResult Index()
        {
            return View();
        }

        // GET: Posts/Details/5
        //public ActionResult Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Post post = db.Posts.Find(id);
        //    if (post == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(post);
        //}

        // GET: Posts/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        // POST: Posts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Create(Post post)
        {
            if (ModelState.IsValid)
            {
                string id = User.Identity.GetUserId();
                ApplicationUser user = db.Users.FirstOrDefault(x => x.Id == id);
                post.Autor = user;
                post.DateOfCreate = DateTime.Now;
                var posT = db.Posts.Add(post);
                db.SaveChanges();
                return PartialView("CreateResult", posT);
            }

            return View();
        }

        // GET: Posts/Edit/5
        [HttpPost]
        public void Edit(Post post_changes)
        {
            try
            {
                Post new_post = db.Posts.Where(x => x.ID == post_changes.ID).FirstOrDefault();
                new_post.Title = post_changes.Title;
                new_post.Text = post_changes.Text;
                db.SaveChanges();
            }
            catch (Exception exp)
            {
                Console.WriteLine("Ошибка: " + exp.Message);
            }
            //string postTitle = newpost.Title;
            //string postText = newpost.Text;

        }

        public void LikingPost(int postID)
        {
            string currentUser = User.Identity.GetUserId();
            Likes newLike = new Likes();
            newLike.personID = currentUser;
            //newLike.Post =  new Post() { ID = postID };
            newLike.Like = true;
            newLike.postID = postID;
            try
            {
                db.Likes.Add(newLike);
                db.SaveChanges();
            }
            catch (Exception exp)
            {

            }
        }

        public void DislikingPost (int postID)
        {
            string currentUser = User.Identity.GetUserId();
            Likes like = db.Likes.Where(x => x.postID == postID && x.personID == currentUser).FirstOrDefault();
            try
            {
                db.Likes.Remove(like);
                db.SaveChanges();
            }
            catch (Exception exp)
            {

            }
        }

        // POST: Posts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "ID,Title,Text,DateOfCreate")] Post post)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(post).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    return View(post);
        //}

        // GET: Posts/Delete/5
        [HttpPost]
        public void Delete(int? id)
        {
            var post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
        }

        // POST: Posts/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
