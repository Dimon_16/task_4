namespace Lesson_3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changePostModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Likes", "postID", c => c.Int(nullable: false));
            AddColumn("dbo.Likes", "personID", c => c.String());
            AddColumn("dbo.Posts", "Like_ID", c => c.Int());
            CreateIndex("dbo.Posts", "Like_ID");
            AddForeignKey("dbo.Posts", "Like_ID", "dbo.Likes", "ID");
            DropColumn("dbo.Likes", "postNumber");
            DropColumn("dbo.Likes", "personWhoLike");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Likes", "personWhoLike", c => c.String());
            AddColumn("dbo.Likes", "postNumber", c => c.Int(nullable: false));
            DropForeignKey("dbo.Posts", "Like_ID", "dbo.Likes");
            DropIndex("dbo.Posts", new[] { "Like_ID" });
            DropColumn("dbo.Posts", "Like_ID");
            DropColumn("dbo.Likes", "personID");
            DropColumn("dbo.Likes", "postID");
        }
    }
}
