namespace Lesson_3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class change : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Likes", "personWhoLike_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Likes", "postNumber_ID", "dbo.Posts");
            DropIndex("dbo.Likes", new[] { "personWhoLike_Id" });
            DropIndex("dbo.Likes", new[] { "postNumber_ID" });
            AddColumn("dbo.Likes", "postNumber", c => c.Int(nullable: false));
            AddColumn("dbo.Likes", "personWhoLike", c => c.String());
            AddColumn("dbo.Likes", "Like", c => c.Boolean(nullable: false));
            DropColumn("dbo.Likes", "Type");
            DropColumn("dbo.Likes", "personWhoLike_Id");
            DropColumn("dbo.Likes", "postNumber_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Likes", "postNumber_ID", c => c.Int());
            AddColumn("dbo.Likes", "personWhoLike_Id", c => c.String(maxLength: 128));
            AddColumn("dbo.Likes", "Type", c => c.Int(nullable: false));
            DropColumn("dbo.Likes", "Like");
            DropColumn("dbo.Likes", "personWhoLike");
            DropColumn("dbo.Likes", "postNumber");
            CreateIndex("dbo.Likes", "postNumber_ID");
            CreateIndex("dbo.Likes", "personWhoLike_Id");
            AddForeignKey("dbo.Likes", "postNumber_ID", "dbo.Posts", "ID");
            AddForeignKey("dbo.Likes", "personWhoLike_Id", "dbo.AspNetUsers", "Id");
        }
    }
}
