namespace Lesson_3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addNotoficationTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Notifications",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Autor_Id_Id = c.String(maxLength: 128),
                        LikedPost_ID = c.Int(),
                        Person_Id_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.Autor_Id_Id)
                .ForeignKey("dbo.Posts", t => t.LikedPost_ID)
                .ForeignKey("dbo.AspNetUsers", t => t.Person_Id_Id)
                .Index(t => t.Autor_Id_Id)
                .Index(t => t.LikedPost_ID)
                .Index(t => t.Person_Id_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Notifications", "Person_Id_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Notifications", "LikedPost_ID", "dbo.Posts");
            DropForeignKey("dbo.Notifications", "Autor_Id_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Notifications", new[] { "Person_Id_Id" });
            DropIndex("dbo.Notifications", new[] { "LikedPost_ID" });
            DropIndex("dbo.Notifications", new[] { "Autor_Id_Id" });
            DropTable("dbo.Notifications");
        }
    }
}
