namespace Lesson_3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addlikestable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Likes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        personWhoLike_Id = c.String(maxLength: 128),
                        postNumber_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.personWhoLike_Id)
                .ForeignKey("dbo.Posts", t => t.postNumber_ID)
                .Index(t => t.personWhoLike_Id)
                .Index(t => t.postNumber_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Likes", "postNumber_ID", "dbo.Posts");
            DropForeignKey("dbo.Likes", "personWhoLike_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Likes", new[] { "postNumber_ID" });
            DropIndex("dbo.Likes", new[] { "personWhoLike_Id" });
            DropTable("dbo.Likes");
        }
    }
}
