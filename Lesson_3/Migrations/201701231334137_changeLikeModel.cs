namespace Lesson_3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeLikeModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Likes", "Post", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Likes", "Post");
        }
    }
}
