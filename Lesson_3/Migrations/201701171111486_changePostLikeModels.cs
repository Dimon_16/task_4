namespace Lesson_3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changePostLikeModels : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Posts", "Like_ID", "dbo.Likes");
            DropIndex("dbo.Posts", new[] { "Like_ID" });
            AddColumn("dbo.Likes", "Post_ID", c => c.Int());
            CreateIndex("dbo.Likes", "Post_ID");
            AddForeignKey("dbo.Likes", "Post_ID", "dbo.Posts", "ID");
            DropColumn("dbo.Likes", "postID");
            DropColumn("dbo.Posts", "Like_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Posts", "Like_ID", c => c.Int());
            AddColumn("dbo.Likes", "postID", c => c.Int(nullable: false));
            DropForeignKey("dbo.Likes", "Post_ID", "dbo.Posts");
            DropIndex("dbo.Likes", new[] { "Post_ID" });
            DropColumn("dbo.Likes", "Post_ID");
            CreateIndex("dbo.Posts", "Like_ID");
            AddForeignKey("dbo.Posts", "Like_ID", "dbo.Likes", "ID");
        }
    }
}
