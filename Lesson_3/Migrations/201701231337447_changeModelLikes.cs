namespace Lesson_3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeModelLikes : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Likes", "Post_ID", "dbo.Posts");
            DropIndex("dbo.Likes", new[] { "Post_ID" });
            RenameColumn(table: "dbo.Likes", name: "Post_ID", newName: "postID");
            AlterColumn("dbo.Likes", "postID", c => c.Int(nullable: false));
            CreateIndex("dbo.Likes", "postID");
            AddForeignKey("dbo.Likes", "postID", "dbo.Posts", "ID", cascadeDelete: true);
            DropColumn("dbo.Likes", "Post");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Likes", "Post", c => c.String());
            DropForeignKey("dbo.Likes", "postID", "dbo.Posts");
            DropIndex("dbo.Likes", new[] { "postID" });
            AlterColumn("dbo.Likes", "postID", c => c.Int());
            RenameColumn(table: "dbo.Likes", name: "postID", newName: "Post_ID");
            CreateIndex("dbo.Likes", "Post_ID");
            AddForeignKey("dbo.Likes", "Post_ID", "dbo.Posts", "ID");
        }
    }
}
