﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Lesson_3.Models
{
    public class Post
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public DateTime DateOfCreate { get; set; }
        public virtual ApplicationUser Autor { get; set; }
        public virtual List<Likes> Likes { get; set; }
        [NotMapped]
        public bool isLike { get; set; }
    }
}