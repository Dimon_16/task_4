﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lesson_3.Models
{
    public class Likes
    {
        public int ID { get; set; }
        public int postID { get; set; }
        public string personID { get; set; }
        public virtual Boolean Like { get; set; }
    }
}