﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lesson_3.Models
{
    public class Notification
    {
        public int ID { get; set; }
        public virtual ApplicationUser Autor_Id { get; set; }
        public virtual ApplicationUser Person_Id { get; set; }
        public virtual Post LikedPost { get; set; }
    }
}