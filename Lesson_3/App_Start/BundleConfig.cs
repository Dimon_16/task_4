﻿using System.Web;
using System.Web.Optimization;

namespace Lesson_3
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery.signalR-2.2.1.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));
            bundles.Add(new StyleBundle("~/Content/Home1/css").Include(
                        "~/Content/Home1/Index.css"));
            bundles.Add(new ScriptBundle("~/bundles/post").Include(
                "~/Scripts/ButtonDelete.js"));
            bundles.Add(new ScriptBundle("~/bundles/buttonedit").Include(
                "~/Scripts/EditButton.js"));
            bundles.Add(new ScriptBundle("~/bundles/savebutton").Include(
                "~/Scripts/SaveButton.js"));
            bundles.Add(new ScriptBundle("~/bundles/angular-components").Include(
                        "~/Scripts/angular.js",
                        "~/Scripts/angular-animate.js",
                        "~/Scripts/angular-messages.js",
                        "~/Scripts/angular-ui-router.js",
                        "~/Scripts/ng-input.js",
                        "~/Scripts/ui-bootstrap-custom-2.2.0.min.js",
                        "~/Scripts/ui-bootstrap-custom-tpls-2.2.0.min.js",
                        "~/Scripts/angular-toastr.tpls.js",
                        "~/Scripts/ng-croppie.js",
                        "~/Scripts/ng-file-upload-all.min.js",
                        "~/Scripts/ng-file-upload.min.js",
                        "~/Scripts/freewall.js",
                        "~/Scripts/angulargrid.min.js"
                      ));
            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                "~/Scripts/Angular/Home/app.js",
                "~/Scripts/Angular/Home/homeModule.js",
                "~/Scripts/Angular/Home/homeComponent.js",
                "~/Scripts/Angular/Home/homeService.js",
                "~/Scripts/Angular/Home/ShowDate.js",
                "~/Scripts/Angular/Home/HomeDirective.js",
                "~/Scripts/Angular/Home/postComponent.js",
                "~/Scripts/Angular/Home/realTimeService.js",
                "~/Scripts/Angular/Home/appService"));
            bundles.Add(new ScriptBundle("~/bundles/angular-ui").Include(
                "~/Scripts/angular-ui/ui-bootstrap-tpls.js"));
        }
    }
}
