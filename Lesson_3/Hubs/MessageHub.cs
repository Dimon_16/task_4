﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.Entity;
using System.Web;
using Microsoft.AspNet.SignalR;
using Lesson_3.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Lesson_3.Hubs
{
    public class MessageHub : Hub
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        //public void Hello()
        //{
        //    Clients.All.hello();
        //}

        public void LikingPost(int postID)
        {
            string currentUser = Context.User.Identity.GetUserId();
            ApplicationUser user = db.Users.FirstOrDefault(x => x.Id == currentUser);
            Post post = db.Posts.FirstOrDefault(x => x.ID == postID);
            Likes newLike = new Likes();
            newLike.personID = currentUser;
            //newLike.Post =  new Post() { ID = postID };
            newLike.Like = true;
            newLike.postID = postID;
            try
            {
                db.Likes.Add(newLike);
                db.SaveChanges();
                Notification note = new Notification();
                note.Autor_Id = post.Autor;
                note.Person_Id = user;
                note.LikedPost = post;
                db.Notifications.Add(note);
                db.SaveChanges();
            }
            catch (Exception exp)
            {

            }
            Clients.All.likingPost(post);
        }

        public void DislikingPost(int postID)
        {
            string currentUser = Context.User.Identity.GetUserId();
            ApplicationUser user = db.Users.FirstOrDefault(x => x.Id == currentUser);
            Post post = db.Posts.FirstOrDefault(x => x.ID == postID);
            Likes like = db.Likes.Where(x => x.postID == postID && x.personID == currentUser).FirstOrDefault();
            Notification note = db.Notifications.Where(x => x.LikedPost.ID == post.ID && x.Person_Id.Id == currentUser).FirstOrDefault();
            try
            {
                db.Likes.Remove(like);
                db.SaveChanges();
                db.Notifications.Remove(note);
                db.SaveChanges();
            }
            catch (Exception exp)
            {

            }

            Clients.All.dislikingPost(post);
        }

    }
}